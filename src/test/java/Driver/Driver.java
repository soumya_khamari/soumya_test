package Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import Utility.Utility;

public class Driver {

	public static WebDriver driver = null;
	public static ExtentHtmlReporter reporter;
	public static ExtentReports extent;
	
	
	
	
	@BeforeClass
	public static void extentReport() {
		reporter = new ExtentHtmlReporter("./ExtentReport/Extent_Soumya.html");
		extent = new ExtentReports();
		
	}


	@BeforeClass
	public void function3() throws Exception {

		String input = Utility.fetchPropertyValue("browserName").toString();
		String input1 = Utility.fetchPropertyValue("applicationURL").toString();

		if (input.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().deleteAllCookies();

		}

		else if (input.equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.ie.driver", "./Driver/IEDriverServer.exe");
			driver = new InternetExplorerDriver();

		}

		else {
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
			driver = new ChromeDriver();

		}
		driver.get(input1);
		driver.manage().window().maximize();

	}

	@AfterClass
	public void function4() {
		driver.quit();

	}

}
