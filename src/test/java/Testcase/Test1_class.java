package Testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import Utility.Utility;
import Implementation.*;

import Driver.Driver;

public class Test1_class extends Driver {



	@DataProvider(name = "TS1")
	public Object[][] TS1_dataprovider() throws Exception {

		Object[][] arrayObject = Utility.getExcelData("TS1");

		return arrayObject;

	}

	@Test(dataProvider = "TS1")
	public void TS1(String No, String Active, String Scenario, String TestCase, String TC_Description, String URL,
			String Username, String Password) {
		Implementation.Function3(Username, Password);

	}


	
	//============================================================================================================================
	
	@DataProvider(name = "TS2")
	public Object[][] TS2_dataprovider() throws Exception {

		Object[][] arrayObject = Utility.getExcelData("TS2");

		return arrayObject;

	}

	@Test(dataProvider = "TS2")
	public void TS2(String No, String Active, String Scenario, String TestCase, String TC_Description, String URL,
			String Username, String Password) throws Exception {
	Implementation.Function4();

	}

	//============================================================================================================================
	@DataProvider(name = "TS3")
	public Object[][] TS3_dataprovider() throws Exception {

		Object[][] arrayObject = Utility.getExcelData("TS3");

		return arrayObject;

	}

	@Test(dataProvider = "TS3")
	public void TS3(String No, String Active, String Scenario, String TestCase, String TC_Description, String URL,
			String Username, String Password) throws Exception {
	Implementation.Function5();

	}
	
	//====================================================================
	@DataProvider(name = "TS4")
	public Object[][] TS4_dataprovider() throws Exception {

		Object[][] arrayObject = Utility.getExcelData("TS4");

		return arrayObject;

	}
	
	@Test(dataProvider = "TS4")
	public void TS4(String No, String Active, String Scenario, String TestCase, String TC_Description, String URL,
			String Username, String Password) throws Exception {
	Implementation.Function6();
	System.out.println("Hello Debo");

	}
	

	
	
	
	
	
	

}