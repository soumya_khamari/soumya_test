package Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import Driver.Driver;

public class Utility extends Driver {

	
	public static String screenshotpath;
	public static ExtentTest logger;

	public static Object fetchPropertyValue(String key) throws Exception {

		FileInputStream file = new FileInputStream("./Config/config.properties");
		Properties property = new Properties();
		property.load(file);
		return property.get(key);

	}

	public static Object fetchElementRepositary(String key) throws Exception {

		FileInputStream file = new FileInputStream("./Config/ElementRepositary.properties");
		Properties property = new Properties();
		property.load(file);
		return property.get(key);

	}

	public static WebElement AccessByXpath(String Element1_Xpath_key) throws Exception {

		String Element1_Xpath1 = Utility.fetchElementRepositary(Element1_Xpath_key).toString();

		// Utility.explicit_wait_xpath(Element1_Xpath_key, 10);

		WebElement element = driver.findElement(By.xpath(Element1_Xpath1));

		return element;

	}

	public static WebElement AccessByID(String Element1_ID_key) throws Exception {

		String Element1_ID1 = Utility.fetchElementRepositary(Element1_ID_key).toString();

		// Utility.explicit_wait_id(Element1_ID_key, 10);

		WebElement element = driver.findElement(By.id(Element1_ID1));

		return element;

	}

	public static WebElement AccessByClass(String Element1_Class_key) throws Exception {

		String Element1_Class1 = Utility.fetchElementRepositary(Element1_Class_key).toString();

		// Utility.explicit_wait_class(Element1_Class_key, 10);

		WebElement element = driver.findElement(By.className(Element1_Class1));

		return element;

	}

	public static void explicit_wait_xpath(String xpath, int wait_time) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, wait_time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

	}

	public static void explicit_wait_id(String id, int wait_time) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, wait_time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));

	}

	public static void explicit_wait_class(String classname, int wait_time) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, wait_time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(classname)));

	}

	public static void highlight(WebElement ele) throws Exception {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		// use executeScript() method and pass the arguments
		// Here i pass values based on css style. Yellow background color with
		// solid red
		// color border.
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", ele);

	}

	public static void Implicit_wait(int timeinsec) throws Exception {
		driver.manage().timeouts().implicitlyWait(timeinsec, TimeUnit.SECONDS);

	}

	public static void ScrolltoView(WebDriver driver, WebElement ele) throws Exception {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", ele);

	}

	// Screenshot

	public static void ReportHeader(String TestCase) {

		extent.attachReporter(reporter);
		logger = extent.createTest(TestCase);
	}

	public static void ReportLogger(String TestStatus, String Details) throws Exception {

		
		
		if (TestStatus.equalsIgnoreCase("P"))

		{
			screenshotpath=ScreenShot();
			logger.pass(Details, MediaEntityBuilder.createScreenCaptureFromPath(screenshotpath).build());
			

		} else {
			screenshotpath=ScreenShot();
			logger.fail(Details, MediaEntityBuilder.createScreenCaptureFromPath(screenshotpath).build());
		}

		// ****************************************
		// String path="./Screenshots/";

		// ********Flush***************************
		extent.flush();
		// ****************************************
	}

	// Excel

	public static Object[][] getonlyExcelData() throws Exception {

		FileInputStream file = new FileInputStream("./TestData/Testdata.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet loginsheet = workbook.getSheet("Sheet1");
		int numberofdata = loginsheet.getPhysicalNumberOfRows();
		Object testdata[][] = new Object[numberofdata][3];
		for (int i = 0; i < numberofdata; i++) {
			XSSFRow row = loginsheet.getRow(i);
			XSSFCell name = row.getCell(0);
			XSSFCell email = row.getCell(1);
			XSSFCell message = row.getCell(2);
			testdata[i][0] = name.getStringCellValue();
			testdata[i][1] = email.getStringCellValue();
			testdata[i][2] = message.getStringCellValue();
		}

		return testdata;

	}

	public static String[][] getExcelData(String TestCaseName)

	{
		String Active = "Y";
		String[][] arrayExcelData = null;
		int count = 0;
		int k = 0;
		try

		{
			FileInputStream fs = new FileInputStream("./TestData/Testdata.xlsx");
			// FileInputStream fs = new FileInputStream(fileName);
			XSSFWorkbook workbook = new XSSFWorkbook(fs);
			XSSFSheet sh = workbook.getSheet("Sheet1");
			int totalNoOfRows = sh.getPhysicalNumberOfRows();
			int totalNoOfCols = sh.getRow(0).getPhysicalNumberOfCells();

			// Get the No. of Rows with Condition
			for (int x = 1; x < totalNoOfRows; x++)

			{

				// System.out.println(sh.getRow(x).getCell(1).getStringCellValue());
				// System.out.println(sh.getRow(x).getCell(3).getStringCellValue());

				if (sh.getRow(x).getCell(1).getStringCellValue().equalsIgnoreCase(Active)
						&& sh.getRow(x).getCell(3).getStringCellValue().equalsIgnoreCase(TestCaseName))

				{

					count++;

				}

			}

			arrayExcelData = new String[count][totalNoOfCols];

			for (int i = 1; i < totalNoOfRows; i++)

			{

				if (sh.getRow(i).getCell(1).getStringCellValue().equalsIgnoreCase(Active)
						&& sh.getRow(i).getCell(3).getStringCellValue().equalsIgnoreCase(TestCaseName)) {

					for (int j = 0; j < totalNoOfCols; j++)

					{
						arrayExcelData[k][j] = sh.getRow(i).getCell(j).getStringCellValue().trim();
						// System.out.println(arrayExcelData[k][j]);
					}

					k++;

				}

			}

			// workbook.close();

		}

		catch (FileNotFoundException e)

		{
			e.printStackTrace();
		}

		catch (IOException e)

		{
			e.printStackTrace();
			e.printStackTrace();
		}

		return arrayExcelData;
	}

	public static String ScreenShot() throws IOException{
            // below line is just to append the date format with the screenshot name
            // to
            // avoid duplicate names
            String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
            TakesScreenshot ts = (TakesScreenshot) driver;
            File source = ts.getScreenshotAs(OutputType.FILE);
            // after execution, you could see a folder "FailedTestsScreenshots"
            // under src
            // folder
            String screenshotName = "SC";
            String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName + dateName + ".png";
            File finalDestination = new File(destination);
            FileUtils.copyFile(source, finalDestination);
            // Returns the captured file path
            return destination;
	}
	
}
